// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Traits.h"
#include "GameFramework/Actor.h"
#include "Phase.generated.h"

class ACourtier;
class AGameOrchestrator;

UCLASS()
class PARCEL_API APhase : public AActor
{
	GENERATED_BODY()
	
public:	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString Name;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString Description;

	UFUNCTION(BlueprintImplementableEvent)
	bool IsPhaseComplete();

	UFUNCTION(BlueprintImplementableEvent)
	void GivePhaseReward();


	// Library of functions for blueprints to call

	UFUNCTION(BlueprintCallable)
	void AddCourtierToCourt(TSubclassOf<ACourtier> CourtierToGenerate);

	UFUNCTION(BlueprintCallable)
	TArray<ACourtier*> GetAllCourtiersInPlay();

	UPROPERTY(BlueprintReadWrite)
	AGameOrchestrator* Orchestrator;
};
