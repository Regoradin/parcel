// Fill out your copyright notice in the Description page of Project Settings.


#include "Courtier.h"
#include "Party.h"
#include "Traits.h"
#include "GameOrchestrator.h"


ACourtier::ACourtier()
{
}

void ACourtier::BeginPlay()
{
	Super::BeginPlay();

	Traits = InitialTraits;
	this->SetTraitText(GetTraitString());
}

void ACourtier::AffectPartyTraitGain(ETrait& Trait, int& Amount)
{
	if (bIsButler)
	{
		Amount *= 2;
	}
	return;
}

FString ACourtier::GetTraitString()
{
	FString TraitString = FString("");

	Traits.KeySort([](ETrait A, ETrait B) {return A < B; });

	for (auto& Trait : Traits)
	{
		int diff = 0;
		if (TraitDiffs.Contains(Trait.Key))
		{
			diff = TraitDiffs[Trait.Key];
		}
		if (Trait.Value != 0 || diff != 0)
		{			
			TraitString += FString::Printf(TEXT("%s: %d"), TraitToDisplayName(Trait.Key), Trait.Value);
			if (diff != 0)
			{
				TraitString += FString::Printf(TEXT("  %s%d)</>"), diff >= 0 ? TEXT("<Positive>(+") : TEXT("<Negative>("), diff);
			}
			TraitString.Append("\n");
		}
	}

	return TraitString;

}

void ACourtier::ResetTraitDiffs()
{
	TraitDiffs.Reset();
	SetTraitText(GetTraitString());
}

int ACourtier::GetTraitValue(ETrait Trait)
{
	if (Traits.Contains(Trait))
	{
		return Traits[Trait];
	}
	return 0;
}

TArray<ACourtier*> ACourtier::GetOtherCourtiersAtParty()
{	
	TArray<ACourtier*> AllCourtiers = CurrentParty->GetAllCourtiers();

	TArray<ACourtier*> OtherCourtiers;

	for (ACourtier* Courtier : AllCourtiers)
	{
		if (Courtier != this)
		{
			OtherCourtiers.Add(Courtier);
		}
	}

	return OtherCourtiers;
}

void ACourtier::SpreadTraitAtParty(ETrait Trait, int Amount)
{
	TArray<ACourtier*> AllCourtiers = GetOtherCourtiersAtParty();
	//AllCourtiers.Add(this);

	for (ACourtier* Courtier : AllCourtiers)
	{
		if (Courtier)
		{
			Courtier->GainTrait(Trait, Amount);
			OnCourtierGainTrait(Trait, Amount);
		}
	}

	if (CurrentParty != nullptr)
	{
		CurrentParty->OnCourtierSpreadTrait(this, Trait, Amount);
	}
}

void ACourtier::GainTrait(ETrait Trait, int Amount)
{
	
	ETrait oppositeTrait = TraitToOpposite(Trait);
	
	if (Traits.Contains(Trait))
	{
		// Courtier has given trait, add Amount to it
		Traits[Trait] += Amount;
	}
	else if (oppositeTrait != ETrait::DEFAULT && Traits.Contains(oppositeTrait))
	{
		// opposite trait exists (is not DEFAULT) and Courtier currently contains opposite trait, subract amount from it
		Traits[oppositeTrait] -= Amount;
		if (Traits[oppositeTrait] < 0)
		{
			// opposite trait was made negative, fix by adding difference to initial trait and removing opposite trait value
			Traits.Add(Trait, abs(Traits[oppositeTrait]));
			Traits[oppositeTrait] = 0;
		}
	}
	else
	{
		// Courtier doesn't have given trait or opposite trait, add it
		Traits.Add(Trait, Amount);
	}

	if (!TraitDiffs.Contains(Trait))
	{
		TraitDiffs.Emplace(Trait, 0);
	}
	TraitDiffs[Trait] += Amount;


	if (Trait == ETrait::DAMAGE && Traits[Trait] >= 3)
	{
		if (Traits.Contains(ETrait::DEATH)) {
			Traits[ETrait::DEATH] += 1;
			TraitDiffs.Add(ETrait::DEATH, 1);
		}
		else {
			Traits.Add(ETrait::DEATH, 1);
			TraitDiffs.Add(ETrait::DEATH, 1);
		}
		//OnCourtierGainTrait(ETrait::DEATH, 1);
		SetTraitText(GetTraitString());
		Die();
	}

	SetTraitText(GetTraitString());
}

void ACourtier::Die()
{
	if (Orchestrator)
	{
		Orchestrator->MoveToGraveyard(this);
	}

	if (!Traits.Contains(ETrait::DEATH))
	{
		Traits.Emplace(ETrait::DEATH, 0);
	}
	if (!TraitDiffs.Contains(ETrait::DEATH))
	{
		TraitDiffs.Emplace(ETrait::DEATH, 0);
	}
	Traits[ETrait::DEATH] += 1;
	TraitDiffs[ETrait::DEATH] += 1;


	OnDeath();
	if (CurrentParty != nullptr)
	{
		CurrentParty->OnCourtierDie(this);
	}
}

ETrait ACourtier::GetHighestTrait()
{
	ETrait HighestTrait = ETrait::DEFAULT;
	int HighestTraitValue = 0;
	for (auto& Trait : Traits)
	{
		if (Traits[Trait.Key] > HighestTraitValue)
		{
			HighestTrait = Trait.Key;
			HighestTraitValue = Trait.Value;
		}
	}
	return HighestTrait;
}

bool ACourtier::GetIsAtAParty()
{	
	return CurrentParty != nullptr;
}

