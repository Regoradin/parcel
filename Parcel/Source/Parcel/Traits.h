// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

// TODO: add special handling for opposite types and special effect types, likely in Courtier

UENUM(BlueprintType)
enum class ETrait : uint8
{
	DEFAULT,
	AMBITION,
	ARCANE,
	ARTIFACT_SWORD,
	ASSASSIN,
	BEAUTY,
	BLOOD,
	BURN,
	DAMAGE,
	DEATH,
	DEBT,
	DESPAIR,
	DOOM,
	EVIL,
	FAME,
	FISH,
	GOLD,
	GOOD,
	GRACE,
	INCITING_INCIDENT,
	JOY,
	MARRIED,
	MIGHT,
	MYSTERIOUS_ARTIFACT,
	NOBILITY,
	OPPORTUNITY,
	PLAGUE,
	PLOT,
	POISON,
	RELIGION,
	SERENITY,
	SMELLY,
	SMITHING,
	UNREST,
	VALOR,
	VAMPIRE
};

inline const ETrait TraitToOpposite(ETrait trait)
{
	switch (trait)
	{
	case ETrait::DAMAGE:
		return ETrait::DEFAULT;
	case ETrait::VALOR:
		return ETrait::UNREST;
	case ETrait::GOOD:
		return ETrait::EVIL;
	case ETrait::EVIL:
		return ETrait::GOOD;
	case ETrait::DEATH:
		return ETrait::DEFAULT;
	case ETrait::GOLD:
		return ETrait::DEBT;
	case ETrait::ARCANE:
		return ETrait::RELIGION;
	case ETrait::RELIGION:
		return ETrait::ARCANE;
	case ETrait::UNREST:
		return ETrait::VALOR;
	case ETrait::DESPAIR:
		return ETrait::JOY;
	case ETrait::SMITHING:
		return ETrait::DEFAULT;
	case ETrait::FAME:
		return ETrait::DEFAULT;
	case ETrait::FISH:
		return ETrait::DEFAULT;
	case ETrait::ARTIFACT_SWORD:
		return ETrait::DEFAULT;
	case ETrait::GRACE:
		return ETrait::DEFAULT;
	case ETrait::BURN:
		return ETrait::DEFAULT;
	case ETrait::MYSTERIOUS_ARTIFACT:
		return ETrait::DEFAULT;
	case ETrait::PLOT:
		return ETrait::DEFAULT;
	case ETrait::VAMPIRE:
		return ETrait::DEFAULT;
	case ETrait::POISON:
		return ETrait::DEFAULT;
	case ETrait::SMELLY:
		return ETrait::BEAUTY;
	case ETrait::SERENITY:
		return ETrait::DEFAULT;
	case ETrait::JOY:
		return ETrait::DESPAIR;
	case ETrait::INCITING_INCIDENT:
		return ETrait::DEFAULT;
	case ETrait::BEAUTY:
		return ETrait::SMELLY;
	case ETrait::ASSASSIN:
		return ETrait::DEFAULT;
	case ETrait::OPPORTUNITY:
		return ETrait::DEFAULT;
	case ETrait::DOOM:
		return ETrait::DEFAULT;
	case ETrait::AMBITION:
		return ETrait::DEFAULT;
	case ETrait::NOBILITY:
		return ETrait::DEFAULT;
	case ETrait::MIGHT:
		return ETrait::DEFAULT;
	case ETrait::PLAGUE:
		return ETrait::DEFAULT;
	case ETrait::DEBT:
		return ETrait::GOLD;
	case ETrait::BLOOD:
		return ETrait::DEFAULT;
	case ETrait::MARRIED:
		return ETrait::DEFAULT;

	}
	return ETrait::DEFAULT;
}

inline const TCHAR* TraitToDisplayName(ETrait trait)
{
	switch (trait)
	{
	case ETrait::DAMAGE:
		return TEXT("Damage");
	case ETrait::VALOR:
		return TEXT("Valor");
	case ETrait::GOOD:
		return TEXT("Good");
	case ETrait::EVIL:
		return TEXT("Evil");
	case ETrait::DEATH:
		return TEXT("Death");
	case ETrait::GOLD:
		return TEXT("Gold");
	case ETrait::ARCANE:
		return TEXT("Arcane");
	case ETrait::RELIGION:
		return TEXT("Religion");
	case ETrait::UNREST:
		return TEXT("Unrest");
	case ETrait::DESPAIR:
		return TEXT("Despair");
	case ETrait::SMITHING:
		return TEXT("Smithing");
	case ETrait::FAME:
		return TEXT("Fame");
	case ETrait::FISH:
		return TEXT("Fish");
	case ETrait::ARTIFACT_SWORD:
		return TEXT("Artifact Sword");
	case ETrait::GRACE:
		return TEXT("Grace");
	case ETrait::BURN:
		return TEXT("Burn");
	case ETrait::MYSTERIOUS_ARTIFACT:
		return TEXT("Mysterious Artifact");
	case ETrait::PLOT:
		return TEXT("Plot");
	case ETrait::VAMPIRE:
		return TEXT("Vampire");
	case ETrait::POISON:
		return TEXT("Poison");
	case ETrait::SMELLY:
		return TEXT("Smelly");
	case ETrait::SERENITY:
		return TEXT("Serenity");
	case ETrait::JOY:
		return TEXT("Joy");
	case ETrait::INCITING_INCIDENT:
		return TEXT("Inciting Incident");
	case ETrait::BEAUTY:
		return TEXT("Beauty");
	case ETrait::ASSASSIN:
		return TEXT("Assassin");
	case ETrait::OPPORTUNITY:
		return TEXT("Opportunity");
	case ETrait::DOOM:
		return TEXT("Doom");
	case ETrait::AMBITION:
		return TEXT("Ambition");
	case ETrait::NOBILITY:
		return TEXT("Nobility");
	case ETrait::MIGHT:
		return TEXT("Might");
	case ETrait::PLAGUE:
		return TEXT("The Plague");
	case ETrait::DEBT:
		return TEXT("Debt");
	case ETrait::BLOOD:
		return TEXT("Blood");
	case ETrait::MARRIED:
		return TEXT("Married");

	}
	return TEXT("Unknown Trait");
}