// Fill out your copyright notice in the Description page of Project Settings.


#include "Party.h"
#include "Courtier.h"
#include "GameOrchestrator.h"
#include "Traits.h"

// Sets default values
AParty::AParty()
{
	CurrentCourtiers.Init(nullptr, 0);
}

void AParty::EvaluateCourtiersAndPartyEffects()
{
	for (int i = 0; i < CurrentCourtiers.Num(); i++)
	{
		CurrentCourtiers[i]->EvaluateCourtierEffect();
	}
	EvaluatePartyEffect();
}

void AParty::AddCourtier(ACourtier* Courtier)
{
	if (CurrentCourtiers.Num() < CourtierSlots) {
		CurrentCourtiers.Emplace(Courtier);
		Courtier->CurrentParty = this;
	}
}

void AParty::RemoveCourtier(ACourtier* Courtier)
{
	CurrentCourtiers.Remove(Courtier);
	Courtier->CurrentParty = nullptr;
}

void AParty::GainTraitForAllCourtier(ETrait Trait, int amount)
{
	for (int32 Index = 0; Index != CurrentCourtiers.Num(); ++Index)
	{
		CurrentCourtiers[Index]->AffectPartyTraitGain(Trait, amount);
	}
	for (int32 Index = 0; Index != CurrentCourtiers.Num(); ++Index)
	{
		if (CurrentCourtiers[Index]->bIsPrincess)
		{
			//Special case for Overly Enthusiastic Princess
			CurrentCourtiers[Index]->GainTrait(Trait, amount * 2);
		}
		else
		{
			CurrentCourtiers[Index]->GainTrait(Trait, amount);
		}
	}
}

void AParty::GainTraitForCourtierAtIndex(ETrait Trait, int amount, int index)
{
	for (int32 Index = 0; Index != CurrentCourtiers.Num(); ++Index)
	{
		CurrentCourtiers[Index]->AffectPartyTraitGain(Trait, amount);
	}
	if (CurrentCourtiers[index]->bIsPrincess)
	{
		//Special case for Overly Enthusiastic Princess
		CurrentCourtiers[index]->GainTrait(Trait, amount * 2);
	}
	else
	{
		CurrentCourtiers[index]->GainTrait(Trait, amount);
	}
}

TArray<ACourtier*> AParty::GetAllCourtiers()
{
	return CurrentCourtiers;
}

ACourtier* AParty::GetCourtierAtIndex(int index)
{
	//access current courtiers
	return CurrentCourtiers[index];
}

ACourtier* AParty::GetCourtierWithHighestTrait(ETrait Trait)
{
	ACourtier* HighestCourtier = CurrentCourtiers[0];
	int HighestValue = CurrentCourtiers[0]->GetTraitValue(Trait);

	for (ACourtier* Courtier : CurrentCourtiers)
	{
		int Value = Courtier->GetTraitValue(Trait);
		if (Value > HighestValue)
		{
			HighestCourtier = Courtier;
			HighestValue = Value;
		}
	}

	return HighestCourtier;
}

ACourtier* AParty::GetCourtierWithLowestTrait(ETrait Trait)
{
	ACourtier* LowestCourtier = CurrentCourtiers[0];
	int LowestValue = CurrentCourtiers[0]->GetTraitValue(Trait);

	for (ACourtier* Courtier : CurrentCourtiers)
	{
		int Value = Courtier->GetTraitValue(Trait);
		if (Value > LowestValue)
		{
			LowestCourtier = Courtier;
			LowestValue = Value;
		}
	}

	return LowestCourtier;
}

void AParty::AddNewCourtierToCourt(TSubclassOf<ACourtier> CourtierToGenerate)
{
	Orchestrator->AddCourtierToCourt(GetWorld()->SpawnActor<ACourtier>(CourtierToGenerate));
}

bool AParty::IsTraitPresent(ETrait Trait)
{
	for (ACourtier* Courtier : CurrentCourtiers)
	{
		if (Courtier->GetTraitValue(Trait)) {
			return true;
		}
	}
	return false;
}

int AParty::GetTraitTotalAtParty(ETrait Trait)
{
	ETrait OppositeTrait = TraitToOpposite(Trait);

	int TraitTotal = 0;
	for (ACourtier* Courtier : CurrentCourtiers)
	{
		TraitTotal += Courtier->GetTraitValue(Trait);
		if (OppositeTrait != ETrait::DEFAULT)
		{
			TraitTotal -= Courtier->GetTraitValue(OppositeTrait);
		}
	}

	return TraitTotal;
}

void AParty::Reset()
{
	CurrentCourtiers.Empty();
}