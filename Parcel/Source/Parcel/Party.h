// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Traits.h"
#include "Party.generated.h"

class AGameOrchestrator;
class ACourtier;

UCLASS()
class PARCEL_API AParty : public AActor
{
	GENERATED_BODY()

public:
	AParty();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int CourtierSlots;

	void EvaluateCourtiersAndPartyEffects();

	/// <summary>
	/// Hook to affect the trait spread of courtier. This is called by the courtiers when they spread traits.
	/// </summary>
	UFUNCTION(BlueprintImplementableEvent)
	void AffectPartyTraitGain(ETrait& Trait, int& Amount);


	UFUNCTION(BlueprintCallable)
	void AddCourtier(ACourtier* Courtier);

	UFUNCTION(BlueprintCallable)
	void RemoveCourtier(ACourtier* Courtier);


	UFUNCTION(BlueprintImplementableEvent)
	void EvaluatePartyEffect();

	// Functions for blueprints to build specific party effects from

	UFUNCTION(BlueprintCallable)
	void GainTraitForAllCourtier(ETrait Trait, int amount);

	UFUNCTION(BlueprintCallable)
	void GainTraitForCourtierAtIndex(ETrait Trait, int amount, int index);

	UFUNCTION(BlueprintCallable)
	TArray<ACourtier*> GetAllCourtiers();

	UFUNCTION(BlueprintCallable)
	ACourtier* GetCourtierAtIndex(int index);

	UFUNCTION(BlueprintCallable)
	ACourtier* GetCourtierWithHighestTrait(ETrait Trait);

	UFUNCTION(BlueprintCallable)
	ACourtier* GetCourtierWithLowestTrait(ETrait Trait);

	UFUNCTION(BlueprintCallable)
	void AddNewCourtierToCourt(TSubclassOf<ACourtier> CourtierToGenerate);

	UFUNCTION(BlueprintCallable)
	bool IsTraitPresent(ETrait Trait);

	UFUNCTION(BlueprintCallable)
	int GetTraitTotalAtParty(ETrait Trait);

	UPROPERTY(BlueprintReadWrite)
	AGameOrchestrator* Orchestrator;

	void Reset();

	UPROPERTY(EditAnywhere)
	bool bIsNecromancerGala;

	// Functions used for some specific parties

	UFUNCTION(BlueprintImplementableEvent)
	void OnCourtierSpreadTrait(ACourtier* Courtier, ETrait Trait, int Amount);

	UFUNCTION(BlueprintImplementableEvent)
	void OnCourtierDie(ACourtier* Courtier);

private:
	TArray<ACourtier*> CurrentCourtiers;

};