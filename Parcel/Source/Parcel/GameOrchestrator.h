// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameOrchestrator.generated.h"

class AGoal;
class ACourtier;
class AParty;
class AVictoryAudienceSpawner;

UCLASS()
class PARCEL_API AGameOrchestrator : public AActor
{
	GENERATED_BODY()
	
public:	
	AGameOrchestrator();

	UPROPERTY(EditAnywhere)
	int PartiesPerTurn = 2;

	UPROPERTY(EditAnywhere)
	int ExtraCharacters = 2;

	UPROPERTY(EditAnywhere)
	TSoftObjectPtr<UWorld> WinLevel;

	UPROPERTY(EditAnywhere)
	TSoftObjectPtr<UWorld> LoseLevel;

	UPROPERTY(EditAnywhere)
	TSubclassOf<AVictoryAudienceSpawner> AudienceSpawnerClass;

	UFUNCTION(BlueprintCallable)
	void AddCourtierToCourt(ACourtier* Courtier);

	UFUNCTION(BlueprintCallable)
	TArray<ACourtier*> GetCourtCourtiers();


	UFUNCTION(BlueprintCallable)
	void Initialize(TArray<AGoal*> Goals, TArray<AParty*> Parties, TArray<ACourtier*> Courtiers);

	UFUNCTION(BlueprintCallable)
	void DrawParties();
	
	/// <summary>
	/// Whether or not the player can get more Courtiers from the Outsiders or the Court (or other places)
	/// </summary>
	UFUNCTION(BlueprintCallable)
	bool CanAddCourtiers();

	UFUNCTION(BlueprintCallable)
	void AddCourtierFromCourt();

	UFUNCTION(BlueprintCallable)
	void AddCourtierFromOutsiders();

	UFUNCTION(BlueprintCallable)
	void AddCourtierFromGraveyard();

	/// <summary>
	/// Whether or not the player can end the turn and evaluate all parties
	/// </summary>
	UFUNCTION(BlueprintCallable)
	bool CanEndTurn();

	UFUNCTION(BlueprintCallable)
	bool CanAddGraveyard();

	UFUNCTION(BlueprintCallable)
	void MoveToGraveyard(ACourtier* Courtier);

	UFUNCTION(BlueprintCallable)
	void EndTurn();

	UFUNCTION(BlueprintCallable)
	void Cleanup();

	UFUNCTION(BlueprintCallable)
	void TriggerFinaleParty(TSubclassOf<AParty> FinaleParty);

	// This courtier will be added to the top of the stack to draw for next round. Will be drawn in the same order as they are added.
	UFUNCTION(BlueprintCallable)
	void AddRequiredCourtierForNextRound(ACourtier* RequiredCourtier);

	UFUNCTION(BlueprintCallable)
	void Win();

	UFUNCTION(BlueprintCallable)
	void Lose();

	UFUNCTION(BlueprintCallable)
	int	GetCountTotalCourtiers();

	UFUNCTION(BlueprintCallable)
	int GetCountInCourt();

	UFUNCTION(BlueprintCallable)
	int GetCountInOutsiders();

	UFUNCTION(BlueprintCallable)
	int GetCountInGraveyard();

	// Things in play in the current turn
	UPROPERTY(BlueprintReadOnly)
	TArray<AParty*> CurrentParties;
	UPROPERTY(BlueprintReadOnly)
	TArray<ACourtier*> CurrentCourtiers;


private:
	TArray<AGoal*> Goals;
	TArray<AParty*> RemainingParties;

	TArray<ACourtier*> Outsiders;
	TArray<ACourtier*> Court;
	TArray<ACourtier*> Graveyard;

	TSubclassOf<AParty> FinalePartyToSpawn;
	bool bShouldSpawnFinaleParty;
	TArray<ACourtier*> RequiredCourtiers;

	int TotalCourtiersCount;

	// Remove a random element from an array
	template<typename T>
	T DrawRandom(TArray<T> &Deck);

	// End-of-turn evaluations
	void EvaluateCurrentParties();
	void EvaluateCurrentTraitEffects();
	void EvaluateCurrentGoals();
	void EvaluateCourtiersNotAtParties();

	bool bOverrideCanAddGraveyard = false;

};

template<typename T>
inline T AGameOrchestrator::DrawRandom(TArray<T> &Deck)
{
	int index = FMath::RandRange(0, Deck.Num() - 1);
	T result = Deck[index];
	Deck.RemoveAt(index);
	return result;
}
