// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Goal.generated.h"

class APhase;
class AGameOrchestrator;

UCLASS()
class PARCEL_API AGoal : public AActor
{
	GENERATED_BODY()
	
public:	
	AGoal();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<APhase*> Phases;

	//Check if the current phase is complete, and if so gain the reward and move to the next phase.
	UFUNCTION(BlueprintCallable)
	void EvaluateGoal();

	UPROPERTY(BlueprintReadWrite)
	AGameOrchestrator* Orchestrator;

	UFUNCTION(BlueprintImplementableEvent)
	void OnChangePhase(APhase* NewPhase);

private:
	int CurrentPhase;
};
