// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Traits.h"
#include "Courtier.generated.h"

class AGameOrchestrator;
class AParty;

UCLASS()
class PARCEL_API ACourtier : public AActor
{
	GENERATED_BODY()

public:	
	ACourtier();

	UFUNCTION(BlueprintImplementableEvent)
	void EvaluateCourtierEffect();

	UFUNCTION(BlueprintImplementableEvent)
	void EvaluateCourtierEffectNotAtParty();

	UPROPERTY(BlueprintReadOnly)
	TMap<ETrait, int> Traits;

	UPROPERTY(EditAnywhere)
	TMap<ETrait, int> InitialTraits;

	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	bool bIsButler;
	UPROPERTY(EditAnywhere)
	bool bIsPrincess;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bMustBeInvited;

	// Hook to affect the trait gain of parties. This is called by the parties when they give traits to courtiers.
	void AffectPartyTraitGain(ETrait &Trait, int &Amount);

	
	UFUNCTION(BlueprintCallable)
	FString GetTraitString(); //Strong arugment that this should be an FText, but the formatting functions are a bit complex

	// Reset the trait diff tracking, which is used to display trait gains and losses over the course of a turn
	void ResetTraitDiffs();

	/*
This is blueprint implementable to allow the text object to get set up in the editor, but for this to be initiated in C++.
Not sure if it's a goood pattern, but here we are
*/
	UFUNCTION(BlueprintImplementableEvent)
	void SetTraitText(const FString& text);
	
	// Functions for blueprints to build specific character effects from

	UFUNCTION(BlueprintCallable)
	int GetTraitValue(ETrait Trait);

	UFUNCTION(BlueprintCallable)
	TArray<ACourtier*> GetOtherCourtiersAtParty();

	/// <summary>
	/// Spread this trait to all other Courtiers at the current party
	/// </summary>
	UFUNCTION(BlueprintCallable)
	void SpreadTraitAtParty(ETrait Trait, int Amount);

	/// <summary>
	/// Gain the given Trait in the given amount
	/// </summary>
	UFUNCTION(BlueprintCallable)
	void GainTrait(ETrait Trait, int Amount);

	/// <summary>
	/// This courtier gains 1 Death Trait, and is killed
	/// </summary>
	UFUNCTION(BlueprintCallable)
	void Die();

	UFUNCTION(BlueprintImplementableEvent)
	void OnDeath();

	UFUNCTION(BlueprintCallable)
	ETrait GetHighestTrait();

	UFUNCTION(BlueprintCallable)
	bool GetIsAtAParty();

	UPROPERTY(BlueprintReadWrite)
	AGameOrchestrator* Orchestrator;

	UPROPERTY(BlueprintReadOnly)
	AParty* CurrentParty;

	UFUNCTION(BlueprintImplementableEvent)
	void OnCourtierGainTrait(ETrait Trait, int Amount);

private:
	TMap<ETrait, int> TraitDiffs;
};
