// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "VictoryAudienceSpawner.generated.h"

class ACourtier;

UCLASS()
class PARCEL_API AVictoryAudienceSpawner : public AActor
{
	GENERATED_BODY()

public:
	 void Init(TArray<ACourtier*> AudienceCourtiers);

	 UFUNCTION(BlueprintImplementableEvent)
	 void OnInited();

	 UPROPERTY(BlueprintReadOnly);
	 TArray<ACourtier*> Audience;
};