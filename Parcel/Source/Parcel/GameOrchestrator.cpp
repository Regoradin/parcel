// Fill out your copyright notice in the Description page of Project Settings.


#include "GameOrchestrator.h"
#include "Party.h"
#include "Courtier.h"
#include "Goal.h"
#include "Kismet/GameplayStatics.h"
#include "VictoryAudienceSpawner.h"

class ACourtier;
// Sets default values
AGameOrchestrator::AGameOrchestrator()
{
	bShouldSpawnFinaleParty = false;
	FinalePartyToSpawn = nullptr;
}

void AGameOrchestrator::Initialize(TArray<AGoal*> _Goals, TArray<AParty*> _Parties, TArray<ACourtier*> _Courtiers)
{
	Goals = _Goals;
	RemainingParties = _Parties;
	Outsiders = _Courtiers;
	TotalCourtiersCount = _Courtiers.Num();

	Court.Init(nullptr, 0);
	Graveyard.Init(nullptr, 0);
	
	// Things in play in the current turn
	CurrentParties.Init(nullptr, 0);
	CurrentCourtiers.Init(nullptr, 0);
	RequiredCourtiers.Init(nullptr, 0);
}

void AGameOrchestrator::AddCourtierToCourt(ACourtier* Courtier) {
	Court.Emplace(Courtier);
}

TArray<ACourtier*> AGameOrchestrator::GetCourtCourtiers() {
	return Court;
}

void AGameOrchestrator::DrawParties()
{

	if (bShouldSpawnFinaleParty && FinalePartyToSpawn != nullptr) 
	{
		AParty* FinaleParty = GetWorld()->SpawnActor<AParty>(FinalePartyToSpawn);
		CurrentParties.Emplace(FinaleParty);
		return;
	}

	if (RemainingParties.Num() < PartiesPerTurn)
	{
		UE_LOG(LogTemp, Log, TEXT("Ran out of Parties!"));
		Lose();
		return;
	}

	for (int i = 0; i < PartiesPerTurn; i++)
	{
		CurrentParties.Add(DrawRandom(RemainingParties));
	}

	for (int i = 0; i < CurrentParties.Num(); i++)
	{
		if (CurrentParties[i]->bIsNecromancerGala)
		{
			bOverrideCanAddGraveyard = true;
		}
	}
}

// Step 2.1 - Validate that player has drawn all courtiers for the turn
bool AGameOrchestrator::CanAddCourtiers()
{
	int TotalPartySlots = 0;
	for (int i = 0; i < CurrentParties.Num(); i++)
	{
		TotalPartySlots += CurrentParties[i]->CourtierSlots;
	}

	return CurrentCourtiers.Num() < TotalPartySlots + ExtraCharacters;
}

void AGameOrchestrator::AddCourtierFromCourt()
{
	if (RequiredCourtiers.Num() > 0)
	{
		CurrentCourtiers.Add(RequiredCourtiers[0]);
		RequiredCourtiers.RemoveAt(0);
		return;
	}
	if (CanAddCourtiers() && Court.Num() > 0) {
		CurrentCourtiers.Add(DrawRandom(Court));
	}
}

void AGameOrchestrator::AddCourtierFromOutsiders()
{
	if (RequiredCourtiers.Num() > 0)
	{
		CurrentCourtiers.Add(RequiredCourtiers[0]);
		RequiredCourtiers.RemoveAt(0);
		return;
	}
	if (CanAddCourtiers() && Outsiders.Num() > 0) {
		CurrentCourtiers.Add(DrawRandom(Outsiders));
	}
}

void AGameOrchestrator::AddCourtierFromGraveyard()
{
	if (CanAddGraveyard() && CanAddCourtiers()  && Graveyard.Num() > 0) {
		CurrentCourtiers.Add(DrawRandom(Graveyard));
	}
}

bool AGameOrchestrator::CanEndTurn()
{
	bool UnassignedPartySeatExists = false;

	for (int i = 0; i < CurrentParties.Num(); i++)
	{
		if (CurrentParties[i]->GetAllCourtiers().Num() < CurrentParties[i]->CourtierSlots)
		{
			return false;
		}
		if (CanAddCourtiers())
		{
			return false;
		}
	}
	for (ACourtier* Courtier : CurrentCourtiers)
	{
		if (Courtier->bMustBeInvited && Courtier->CurrentParty == nullptr)
		{
			return false;
		}
	}

	return true;
}

// TODO: Special events that allow drawing from graveyard
bool AGameOrchestrator::CanAddGraveyard()
{
	return bOverrideCanAddGraveyard;
}

void AGameOrchestrator::MoveToGraveyard(ACourtier* Courtier)
{
	Graveyard.Emplace(Courtier);
}

// Step 3 - Player Ends Turn
void AGameOrchestrator::EndTurn()
{
	UE_LOG(LogTemp, Log, TEXT("Turn ended. Begining evaluations"));
	EvaluateCurrentParties();
	EvaluateCourtiersNotAtParties();
	EvaluateCurrentTraitEffects();
	EvaluateCurrentGoals();
}


// Step 7 - Cleanup Courtiers and Parties from current turn
void AGameOrchestrator::Cleanup()
{
	UE_LOG(LogTemp, Log, TEXT("Cleaning up turn"));

	// Step 7.1 - Move current courtiers to the court deck
	for (int i = 0; i < CurrentCourtiers.Num(); i++)
	{
		CurrentCourtiers[i]->CurrentParty = nullptr;
		CurrentCourtiers[i]->ResetTraitDiffs();
		if (!Graveyard.Contains(CurrentCourtiers[i]))
		{
			Court.Emplace(CurrentCourtiers[i]);
		}
	}

	for (int i = 0; i < CurrentParties.Num(); i++)
	{
		CurrentParties[i]->Reset();
	}

	// Step 7.3 - Clear current parties
	CurrentParties.Empty();
	CurrentCourtiers.Empty();

	bOverrideCanAddGraveyard = false;
}

void AGameOrchestrator::TriggerFinaleParty(TSubclassOf<AParty> FinaleParty)
{
	bShouldSpawnFinaleParty = true;
	FinalePartyToSpawn = FinaleParty;
}

void AGameOrchestrator::AddRequiredCourtierForNextRound(ACourtier* RequiredCourtier)
{
	RequiredCourtiers.Emplace(RequiredCourtier);
}

void AGameOrchestrator::Win()
{	
	UE_LOG(LogTemp, Log, TEXT("YOU WIN!"));
	UGameplayStatics::OpenLevelBySoftObjectPtr(this, WinLevel, true, "");
}

void AGameOrchestrator::Lose()
{
	UE_LOG(LogTemp, Log, TEXT("YOU LOSE!"));
	UGameplayStatics::OpenLevelBySoftObjectPtr(this, LoseLevel, true, "");
}

int AGameOrchestrator::GetCountTotalCourtiers()
{
	return TotalCourtiersCount;
}

int AGameOrchestrator::GetCountInCourt()
{
	return Court.Num();
}

int AGameOrchestrator::GetCountInOutsiders()
{
	return Outsiders.Num();
}

int AGameOrchestrator::GetCountInGraveyard()
{
	return Graveyard.Num();
}



// ---- Private Functions ----

// Step 4 - Evaluate each party
void AGameOrchestrator::EvaluateCurrentParties()
{
	UE_LOG(LogTemp, Log, TEXT("Evaluating Current Parties"));
	for (int i = 0; i < CurrentParties.Num(); i++)
	{
		CurrentParties[i]->EvaluateCourtiersAndPartyEffects();
	}
}

// Step 5 - Evaluate traits with effects
void AGameOrchestrator::EvaluateCurrentTraitEffects()
{
	UE_LOG(LogTemp, Log, TEXT("Evaluating Current Trait Effects"));
	// TODO: It seems that there is no interface for this quite yet
}

// Step 6 - Evaluate each goal
void AGameOrchestrator::EvaluateCurrentGoals()
{
	UE_LOG(LogTemp, Log, TEXT("Evaluating Current Goals"));
	for (int i = 0; i < Goals.Num(); i++)
	{
		Goals[i]->EvaluateGoal();
	}
}

void AGameOrchestrator::EvaluateCourtiersNotAtParties()
{
	for (int i = 0; i < CurrentCourtiers.Num(); i++)
	{
		if (!CurrentCourtiers[i]->GetIsAtAParty())
		{
			CurrentCourtiers[i]->EvaluateCourtierEffectNotAtParty();
		}
	}
}
