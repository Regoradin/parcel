// Fill out your copyright notice in the Description page of Project Settings.


#include "Phase.h"
#include "GameOrchestrator.h"
#include "Courtier.h"

void APhase::AddCourtierToCourt(TSubclassOf<ACourtier> CourtierToGenerate)
{
	Orchestrator->AddCourtierToCourt(GetWorld()->SpawnActor<ACourtier>(CourtierToGenerate));
}

TArray<ACourtier*> APhase::GetAllCourtiersInPlay()
{
	TArray<ACourtier*> CourtiersInPlay;
	CourtiersInPlay.Init(nullptr, 0);

	for (auto* Courtier : Orchestrator->CurrentCourtiers)
	{
		if (Courtier->GetIsAtAParty())
		{
			CourtiersInPlay.Emplace(Courtier);
		}
	}

	return CourtiersInPlay;
}
