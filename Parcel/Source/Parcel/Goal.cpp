// Fill out your copyright notice in the Description page of Project Settings.


#include "Goal.h"
#include "Phase.h"

// Sets default values
AGoal::AGoal()
{
	CurrentPhase = 0;
}

void AGoal::EvaluateGoal()
{
	if (CurrentPhase < Phases.Num() && Phases[CurrentPhase]->IsPhaseComplete()) {
		Phases[CurrentPhase]->GivePhaseReward();
		CurrentPhase++;

		OnChangePhase(CurrentPhase < Phases.Num() ? Phases[CurrentPhase] : nullptr);
	}
}

